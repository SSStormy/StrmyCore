﻿using System;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;

namespace StrmyCore
{
    public static class Utils
    {
        /// <summary>
        /// Returns the bit at the given index in the given byte value.
        /// </summary>
        /// <param name="value">The byte value from which we will be extracting the given bit at the given index from.</param>
        /// <param name="bitIndex">The index of the bit in the given byte value which we will return.</param>
        /// <returns>The bit at the given index from the given byte value.</returns>
        public static byte GetBit(byte value, byte bitIndex) => (byte)(value & (1 << bitIndex - 1));

        /// <summary>
        /// Swaps two values.
        /// </summary>
        public static void Swap<T>(ref T t1, ref T t2)
        {
            T obj = t1;
            t1 = t2;
            t2 = obj;
        }

        /// <summary>
        /// Clamps two IComparables to the  given min and max values
        /// </summary>
        public static T Clamp<T>(T value, T min, T max) where T : IComparable<T>
        {
            if (value.CompareTo(max) > 0)
                return max;
            if (value.CompareTo(min) < 0)
                return min;

            return value;
        }

        // http://stackoverflow.com/questions/2247095/c-sharp-enum-parse-generics
        /// <summary>
        /// Converts the string representation of the name or numeric value of one or
        /// more enumerated constants to an equivalent enumerated object.
        /// </summary>
        /// <typeparam name="TEnum">An enumeration type.</typeparam>
        /// <param name="value">A string containing the name or value to convert.</param>
        /// <returns>An object of type TEnum whose value is represented by value</returns>
        /// <exception cref="System.ArgumentNullException">enumType or value is null.</exception>
        /// <exception cref="System.OverflowException">value is outside the range of the underlying type of enumType.</exception>
        public static TEnum EnumParse<TEnum>(string value) where TEnum : struct
            => (TEnum) Enum.Parse(typeof (TEnum), value);

        /// <summary>
        /// Returns the last folder or file in dir.
        /// </summary>
        public static string GetFilename(string dir)
        {
            return dir.Remove(0, dir.LastIndexOf(Path.DirectorySeparatorChar) + 1);
        }

        /// <summary>
        /// Cleans the file name from any invalid characters.
        /// </summary>
        public static string CleanFileName(string fileName)
        {
            return Path.GetInvalidFileNameChars()
                .Concat(" ")
                .Aggregate(fileName, (current, c) => current.Replace(c.ToString(), string.Empty));
        }

        /// <summary>
        /// Converts a unix timestamp long into a DateTime.
        /// </summary>
        public static DateTime UnixTimeStampToDateTime(long unixTimeStamp)
        {
            DateTime dtDateTime = new DateTime(1970, 1, 1, 0, 0, 0, 0, DateTimeKind.Utc);
            dtDateTime = dtDateTime.AddSeconds(unixTimeStamp).ToLocalTime();
            return dtDateTime;
        }

        [Obsolete("Use AsyncDownloadRaw()", true)]
        public static string DownloadRaw(string url)
        {
            throw new InvalidOperationException();
        }

        public static bool IsValidJson(string strInput)
        {
            try
            {
                JToken.Parse(strInput);
                return true;
            }
            catch
            {
            }
            return false;
        }

        public static async Task<string> AsyncDownloadRaw(string url,
            string useragent = "Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.1")
        {
            using (HttpClient httpClient = new HttpClient())
            {
                try
                {
                    return await httpClient.GetStringAsync(url);
                }
                catch (Exception ex)
                {
                    Logger.WriteLine($"Failed fetching html ex: {ex}");
                }
                return string.Empty;
            }
        }

        public static async Task<int> RunProcessAsync(string fileName, string args)
        {
            using (Process process = new Process
            {
                StartInfo =
                {
                    FileName = fileName,
                    Arguments = args,
                    UseShellExecute = false,
                    CreateNoWindow = true,
                    RedirectStandardOutput = true,
                    RedirectStandardError = true
                },
                EnableRaisingEvents = true,
            })
            {
                return await RunProcessAsync(process).ConfigureAwait(false);
            }
        }

        public static Task<int> RunProcessAsync(Process process)
        {
            TaskCompletionSource<int> tcs = new TaskCompletionSource<int>();

            process.Exited += (s, ea) => tcs.SetResult(process.ExitCode);

            if (!process.Start())
                throw new InvalidOperationException($"Could not start process: {process}");

            return tcs.Task;
        }
    }
}
