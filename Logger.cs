﻿using System;

namespace StrmyCore
{
    public static class Logger
    {
        private static readonly object Lock = new object();

        public static void Write(string text, ConsoleColor fore = ConsoleColor.Gray)
        {
            lock (Lock)
            {
                Console.ForegroundColor = fore;
                Console.Write(text);
                Console.ResetColor();
            }
        }

        public static void FormattedWrite(string type, string text, ConsoleColor fore = ConsoleColor.Gray)
        {
            WriteTime();
            Write($"[{type}] ", fore);
            Write($"{text}{Environment.NewLine}");
        }

        public static void Writeline(string text) => FormattedWrite("Main", text, ConsoleColor.White);
        public static void WriteLine(string text) => Writeline(text);
        private static void WriteTime()
        => Write($"[{DateTime.Now.ToString("yy/MM/dd HH:mm:ss")}] ", ConsoleColor.Green);
    }
}